Source: python-cx-oracle
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 12), dh-python, python3-all-dev, python3-setuptools, python3-sphinx, libodpic-dev
Standards-Version: 4.4.1
Homepage: https://oracle.github.io/python-cx_Oracle/index.html
Vcs-Browser: https://gitlab.com/kalilinux/packages/python-cx-oracle
Vcs-Git: https://gitlab.com/kalilinux/packages/python-cx-oracle.git

Package: python3-cx-oracle
Architecture: any
Depends: ${python3:Depends}, ${misc:Depends}, ${shlibs:Depends}
Suggests: python-cx-oracle-doc
Description: Python interface to Oracle Database (Python 3)
 This package contains a Python extension module that enables access to
 Oracle Database. It conforms to the Python database API 2.0 specification with
 a considerable number of additions and a couple of exclusions.
 .
 This package installs the library for Python 3.

Package: python-cx-oracle-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: Python interface to Oracle Database (common documentation)
 This package contains a Python extension module that enables access to
 Oracle Database. It conforms to the Python database API 2.0 specification with
 a considerable number of additions and a couple of exclusions.
 .
 This is the common documentation package.
